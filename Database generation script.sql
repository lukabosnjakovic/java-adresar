CREATE USER 'java' IDENTIFIED BY '1234';

CREATE DATABASE java;

GRANT ALL PRIVILEGES ON java.* TO java;
FLUSH PRIVILEGES;

USE java;

CREATE TABLE drzave (
	id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    naziv VARCHAR(255) NOT NULL,
    kontinent VARCHAR(255),
    pozivniBroj VARCHAR(255)
);

CREATE TABLE zupanije (
	id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    naziv VARCHAR(255) NOT NULL,
    drzavaID int,
    FOREIGN KEY (drzavaID) REFERENCES drzave(id),
    pozivniBroj VARCHAR(255)
);

CREATE TABLE gradovi (
	id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    naziv VARCHAR(255) NOT NULL,
    zupanijaID int, 
    FOREIGN KEY (zupanijaID) REFERENCES zupanije(id)
);

CREATE TABLE kontakti (
	id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ime VARCHAR(255) NOT NULL,
    prezime VARCHAR(255) NOT NULL,
    ulica VARCHAR(255),
    broj VARCHAR(255),
    dodatak VARCHAR(255),
    gradID int,
    FOREIGN KEY (gradID) REFERENCES gradovi(id),
    kucni VARCHAR(255),
    mobitel VARCHAR(255),
    posao VARCHAR(255)
);


INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Hrvatska','Europa','+385');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Bosna i Hercegovina','Europa','+387');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Slovenija','Europa','+386');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Makedonija','Europa','+389');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Srbija','Europa','+381');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Crna Gora','Europa','+382');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Italija','Europa','+39');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Mađarska','Europa','+36');
INSERT INTO	drzave(naziv, kontinent, pozivniBroj) VALUES ('Austrija','Europa','+43');

INSERT INTO	zupanije(naziv, drzavaID, pozivniBroj) VALUES ('Grad Zagreb',1,'01');
INSERT INTO	zupanije(naziv, drzavaID, pozivniBroj) VALUES ('Zagrebačka',1,'01');
INSERT INTO	zupanije(naziv, drzavaID, pozivniBroj) VALUES ('Dubrovačko-Neretvanska',1,'020');
INSERT INTO	zupanije(naziv, drzavaID, pozivniBroj) VALUES ('Splitsko-Dalmatinska',1,'021');

INSERT INTO gradovi(naziv, zupanijaID) VALUES ( 'Zagreb' , '1' );
INSERT INTO gradovi(naziv, zupanijaID) VALUES ( 'Samobor' , '2' );
INSERT INTO gradovi(naziv, zupanijaID) VALUES ( 'Dubrovnik' , '3' );
INSERT INTO gradovi(naziv, zupanijaID) VALUES ( 'Split' , '4' );

INSERT INTO kontakti(ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) VALUES ( 'Ante' , 'Anić' , 'Anina' , '3' , 'A' , '1' , '3012548' , '098546542' , '');
INSERT INTO kontakti(ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) VALUES ( 'Branko' , 'Brkić' , 'Brkljina' , '7' , 'B' , '1' , '3012548' , '098546542' , '');
INSERT INTO kontakti(ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) VALUES ( 'Cico' , 'Kranjčar' , 'Tratinska' , '15' , '3' , '1' , '3012548' , '098546542' , '');
INSERT INTO kontakti(ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) VALUES ( 'Drago' , 'Krstić' , 'Trg bana Josipa Jelačića' , '1' , 'A' , '1' , '3012548' , '098546542' , '');
INSERT INTO kontakti(ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) VALUES ( 'Marko' , 'Marulić' , 'Gajeva' , '43' , 'C' , '1' , '3012548' , '098546542' , '');
INSERT INTO kontakti(ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) VALUES ( 'Ivo' , 'Ivić' , 'Klaiceva' , '7' , '1' , '1' , '3012548' , '098546542' , '');
