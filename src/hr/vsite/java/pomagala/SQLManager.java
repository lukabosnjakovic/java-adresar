package hr.vsite.java.pomagala;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.JOptionPane;

/**
 * 
 * @author Luka Bošnjaković
 * Klasa namjenjena za rad s bazama, sadrži metode za otvaranje i zatvaranje konekcije sa bazom
 * te metode za slanje i èitanje podataka iz baze
 */
public class SQLManager {

	private Connection con;								
	private String username;// = "java"; 					// Username
	private String password;// = "1234";					// Password
	private String host;// = "localhost:3306/java";		// Host
	
	public SQLManager(){ }	
	
	private void loadParams() {
		Properties props = new Properties();
		InputStream is = null;
		try {
			File f = new File("Adresar.properties");
			is = new FileInputStream(f);
		} 
		catch (Exception e) {
			MyLogger.log.error("Greška kod otvarnja datoteke", e);
			is = null;
		}
		try {

			props.load(is);
			host = props.getProperty("host");
			username = props.getProperty("username");
			password = props.getProperty("password");
		} 
		catch (Exception e) {
		}	
	}
	
	/**
	 * Otvara konekciju prema bazi
	 */
	private void open(){
		loadParams();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://" + host, username, password);	
						
		} catch (ClassNotFoundException ex) {
			MyLogger.log.error("SQL Manager Open:" + ex.getMessage());
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Greška!", JOptionPane.ERROR_MESSAGE);
		} catch (SQLException ex) {
			MyLogger.log.error("SQL Manager Open:" + ex.getMessage());
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Greška!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Zatvara otvotenu konekciju baze
	 */
	private void close(){
		try
		{
			if(!con.isClosed())
				con.close();
		}
		catch(SQLException ex)
		{ 
			MyLogger.log.error("SQL Manager Close:" + ex.getMessage());
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Greška!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Vraća tablicu podataka pročitanih iz baze podataka
	 * @param tablica String
	 * @param parametri Vector<String>
	 * @param whereStmt String
	 * @return Vector<Vector<String>>
	 */
	public Vector<Vector<String>> read(String tablica, Vector<String> parametri, String whereStmt){
		open();
		Statement stmt = null;
		Vector<Vector<String>> table = new Vector<Vector<String>>();
		String querry = "SELECT ";
		for(String p : parametri)
			querry += p + ", ";
		querry = querry.substring(0, querry.lastIndexOf(',')) + " FROM " + tablica +" "+ whereStmt +" LIMIT 100;";
		
		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(querry);
			while (rs.next()) {
				Vector<String> row = new Vector<String>();
				for (String p : parametri)
					row.add(rs.getString(p));
				table.add(row);
			}
		} catch (SQLException e) {
			MyLogger.log.error("SQL Manager read: " + e.getMessage());
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException ex) {
					MyLogger.log.error("SQL Manager read, greska kod zatvaranja konekcije: " + ex.getMessage());
				}
			}
		}		
		close();
		return table;
	}
	
	/**
	 * Vraća tablicu podataka iz baze ručno pisanim upitom
	 * @param naredba Ručno pisani querry
	 * @param parametri Vektor parametara
	 * @return Vector<Vector<String>> Tablica podataka
	 */
	public Vector<Vector<String>> read(String naredba, Vector<String> parametri){
		open();
		Statement stmt = null;
		Vector<Vector<String>> table = new Vector<Vector<String>>();		
		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(naredba);
			while (rs.next()) {
				Vector<String> row = new Vector<String>();
				for (String p : parametri)
					row.add(rs.getString(p));
				table.add(row);
			}
		} catch (SQLException e) {
			MyLogger.log.error("SQL Manager read: " + e.getMessage());
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException ex) {
					MyLogger.log.error("SQL Manager read, greska kod zatvaranja konekcije: " + ex.getMessage());
				}
			}
		}		
		close();
		return table;
	}
	
	/**
	 * Šalje podatke u bazu
	 * @param naredba, Pripremljena naredba (String)
	 * @param parametri, Podaci koji se šalju u mapi (TreeMap<Integer,String>)
	 */
	public void send(String naredba, TreeMap<Integer,String> parametri){
		open();
		PreparedStatement updateSales = null;
		try {
			updateSales = con.prepareStatement(naredba);
			for(Map.Entry<Integer,String> parametar : parametri.entrySet())
				updateSales.setString(parametar.getKey(), parametar.getValue());
			updateSales.executeUpdate();
		} catch (SQLException e) {
			MyLogger.log.error("SQL Manager Send: " + e.getMessage());
		} finally {
			if (updateSales != null) {
				try {
					updateSales.close();
				} catch (SQLException e) {
					MyLogger.log.error("SQL Manager Send: " + e.getMessage());
				}
			}
		}
		close();
	}
	
	public void delete(String tablica, String id){
		open();
		PreparedStatement updateSales = null;
		try {
			updateSales = con.prepareStatement("DELETE FROM " + tablica + " WHERE id = ? ;");
			updateSales.setInt(1, Integer.parseInt(id));
			updateSales.executeUpdate();
		} catch (SQLException e) {
			MyLogger.log.error("SQL Manager Send: " + e.getMessage());
		} finally {
			if (updateSales != null) {
				try {
					updateSales.close();
				} catch (SQLException e) {
					MyLogger.log.error("SQL Manager Send: " + e.getMessage());
				}
			}
		}
		close();
	}
}
