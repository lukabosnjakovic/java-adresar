package hr.vsite.java.pomagala;

import hr.vsite.java.Adresar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Luka Bošnjaković
 * Klasa namjenjena za logiranje grešaka
 */
public class MyLogger {
	public static final Logger log = LoggerFactory.getLogger(Adresar.class);
}
