package hr.vsite.java;

import hr.vsite.java.dialozi.KontaktDialog;
import hr.vsite.java.dialozi.PostavkeDialog;
import hr.vsite.java.forme.DrzaveForma;
import hr.vsite.java.forme.GradoviForma;
import hr.vsite.java.forme.ZupanijeForma;
import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Kontakt;

import java.awt.EventQueue;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Font;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.SpringLayout;

public class Adresar extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtTrazilica;
	private JTable table;
	private JButton btnBrii;
	public String whereStm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Adresar frame = new Adresar("");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Adresar(String where) {
		whereStm = where;
		setTitle("Adresar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 630, 492);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnDatoteka = new JMenu("Datoteka");
		menuBar.add(mnDatoteka);
		
		JMenuItem mntmPostavke = new JMenuItem("Postavke");
		mntmPostavke.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PostavkeDialog dialog = new PostavkeDialog();
				dialog.setVisible(true);
				restartForma("");
			}
		});
		mnDatoteka.add(mntmPostavke);
		
		JMenuItem mntmIzlaz = new JMenuItem("Izlaz");
		mntmIzlaz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		mnDatoteka.add(mntmIzlaz);
		
		JMenu mnPopis = new JMenu("Popis");
		menuBar.add(mnPopis);
		
		JMenuItem mntmGradovi = new JMenuItem("Gradovi");
		mntmGradovi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GradoviForma forma = new GradoviForma();
				forma.setVisible(true);
				forma.dispose();
				restartForma("");
			}
		});
		mnPopis.add(mntmGradovi);
		
		JMenuItem mntmupanije = new JMenuItem("\u017Dupanije");
		mntmupanije.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ZupanijeForma forma = new ZupanijeForma();
				forma.setVisible(true);
				forma.dispose();
				restartForma("");
			}
		});
		mnPopis.add(mntmupanije);
		
		JMenuItem mntmDrave = new JMenuItem("Dr\u017Eave");
		mntmDrave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DrzaveForma forma = new DrzaveForma();
				forma.setVisible(true);
				forma.dispose();
				restartForma("");
			}
		});
		mnPopis.add(mntmDrave);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel lblTrailica = new JLabel("Tra\u017Eilica");
		sl_contentPane.putConstraint(SpringLayout.WEST, lblTrailica, 10, SpringLayout.WEST, contentPane);
		lblTrailica.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(lblTrailica);
		
		txtTrazilica = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblTrailica, 3, SpringLayout.NORTH, txtTrazilica);
		sl_contentPane.putConstraint(SpringLayout.NORTH, txtTrazilica, 13, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, txtTrazilica, 77, SpringLayout.WEST, contentPane);
		txtTrazilica.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtTrazilica.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					aktivirajTrazilicu();
				}
			}
		});
		contentPane.add(txtTrazilica);
		txtTrazilica.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		sl_contentPane.putConstraint(SpringLayout.NORTH, scrollPane, 45, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, scrollPane, 8, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, scrollPane, -8, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, scrollPane, -8, SpringLayout.EAST, contentPane);
		contentPane.add(scrollPane);

		Vector<String> param = new Vector<String>();
		param.add("id");
		param.add("ime");
		param.add("prezime");
		param.add("kucni");
		param.add("mobitel");		
		Vector<Vector<String>> table0 = puniKontakte(param, whereStm);
		param.clear();
		param.add("ID");
		param.add("Ime");
		param.add("Prezime");
		param.add("Tel. Kućni");
		param.add("Mobitel");
		DefaultTableModel model = new DefaultTableModel(table0, param) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int i, int i1) {
				return false; // To change body of generated methods, choose
								// Tools | Templates.
			}

		};
		table = new JTable(model);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnBrii.setEnabled(true);
				if(e.getClickCount() == 2){
					Kontakt kon = new Kontakt();
					int row = table.getSelectedRow();
					Vector<String> param = new Vector<String>();
					param.add("id");
					param.add("ime");
					param.add("prezime");
					param.add("ulica");
					param.add("broj");
					param.add("dodatak");
					param.add("gradID");
					param.add("kucni");
					param.add("mobitel");
					param.add("posao");
					SQLManager baza = new SQLManager();
					Vector<Vector<String>> konta = baza
							.read("kontakti", param, "WHERE id = "
									+ table.getValueAt(row, 0).toString());
					kon.setId(konta.elementAt(0).elementAt(0));
					kon.setIme(konta.elementAt(0).elementAt(1));
					kon.setPrezime(konta.elementAt(0).elementAt(2));
					kon.setUlica(konta.elementAt(0).elementAt(3));
					kon.setBroj(konta.elementAt(0).elementAt(4));
					kon.setdodatak(konta.elementAt(0).elementAt(5));
					kon.setGrad(konta.elementAt(0).elementAt(6));
					kon.setKucni(konta.elementAt(0).elementAt(7));
					kon.setMobitel(konta.elementAt(0).elementAt(8));
					kon.setPosao(konta.elementAt(0).elementAt(9));
					KontaktDialog dialog = new KontaktDialog(kon, true);
					dialog.setVisible(true);
					restartForma("");
				}
			}
		});
		table.setFillsViewportHeight(true);
		table.setFont(new Font("Tahoma", Font.PLAIN, 13));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		JButton btnNovi = new JButton("Novi");
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnNovi, 12, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, btnNovi, -89, SpringLayout.EAST, scrollPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnNovi, 0, SpringLayout.EAST, scrollPane);
		btnNovi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				KontaktDialog dialog = new KontaktDialog(new Kontakt(), false);
				dialog.setVisible(true);
				restartForma("");
				
			}
		});
		contentPane.add(btnNovi);
		
		btnBrii = new JButton("Briši");
		btnBrii.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.getSelectedRow();
				if(btnBrii.isEnabled() && row != -1){
					SQLManager baza = new SQLManager();
					baza.delete("kontakti", table.getValueAt(row, 0).toString());
					restartForma("");
					btnBrii.setEnabled(false);
				}
			}
		});
		sl_contentPane.putConstraint(SpringLayout.WEST, btnBrii, -80, SpringLayout.WEST, btnNovi);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnBrii, 0, SpringLayout.SOUTH, btnNovi);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnBrii, -6, SpringLayout.WEST, btnNovi);
		contentPane.add(btnBrii);
		btnBrii.setEnabled(false);
		
	}
	
	private void aktivirajTrazilicu(){
		String tmp = txtTrazilica.getText();
		if(tmp.length() == 0)
			whereStm = "";
		else
			whereStm = " WHERE ime LIKE '" + tmp  + "%' OR prezime LIKE '" + tmp + "%'";
		restartForma(whereStm);
	}
	
	private Vector<Vector<String>> puniKontakte(Vector<String> param, String where) {
		if(where == null)
			where = "";
		SQLManager baza = new SQLManager();
		Vector<Vector<String>> table = baza
				.read("kontakti", param, where);
		return table;
	}
	
	private void restartForma(String where) {
		Rectangle rec = this.getBounds();
		dispose();
		Adresar dialog = new Adresar(where);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setBounds(rec);
		dialog.setVisible(true);
	}
}
