package hr.vsite.java.dialozi;

import hr.vsite.java.forme.ZupanijeForma;
import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Grad;
import hr.vsite.java.tipovi.Zupanija;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GradDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private JTextField txtId;
	private JTextField txtNaziv;
	private JLabel lblId;
	private JLabel lblNaziv;
	private JTextField txtZupanijaId;
	private JTextField txtZupanijaNaziv;
	private JLabel lblKontinent;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Grad gr = new Grad();
			gr.setId("1");
			gr.setNaziv("Zagreb");
			gr.setZupanija("1");
			GradDialog dialog = new GradDialog(gr, false);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public GradDialog(Grad grad, boolean update) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setBounds(100, 100, 346, 354);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if(txtNaziv.getText() != ""){
							String naredba = "";
							if (update)
								naredba = "UPDATE gradovi SET naziv = ?, zupanijaID = ? WHERE id = "
										+ txtId.getText() + ";";
							else
								naredba = "INSERT INTO gradovi(naziv, zupanijaID) VALUES ( ? , ? );";

							TreeMap<Integer, String> param = new TreeMap<Integer, String>();
							param.put(1, txtNaziv.getText());
							param.put(2, txtZupanijaId.getText());
							SQLManager baza = new SQLManager();
							baza.send(naredba, param);
							dispose();
						}						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.CENTER);
			SpringLayout sl_panel = new SpringLayout();
			panel.setLayout(sl_panel);

			txtId = new JTextField();
			sl_panel.putConstraint(SpringLayout.WEST, txtId, -240,
					SpringLayout.EAST, panel);
			txtId.setEditable(false);
			txtId.setEnabled(false);
			txtId.setFont(new Font("Tahoma", Font.PLAIN, 13));
			sl_panel.putConstraint(SpringLayout.NORTH, txtId, 59,
					SpringLayout.NORTH, panel);
			sl_panel.putConstraint(SpringLayout.EAST, txtId, -29,
					SpringLayout.EAST, panel);
			panel.add(txtId);
			txtId.setColumns(10);

			txtNaziv = new JTextField();
			sl_panel.putConstraint(SpringLayout.NORTH, txtNaziv, 25,
					SpringLayout.SOUTH, txtId);
			sl_panel.putConstraint(SpringLayout.WEST, txtNaziv, 0,
					SpringLayout.WEST, txtId);
			sl_panel.putConstraint(SpringLayout.EAST, txtNaziv, -29,
					SpringLayout.EAST, panel);
			txtNaziv.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtNaziv.selectAll();
				}
			});
			txtNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
			panel.add(txtNaziv);
			txtNaziv.setColumns(10);
			{
				lblId = new JLabel("ID");
				sl_panel.putConstraint(SpringLayout.NORTH, lblId, 60,
						SpringLayout.NORTH, panel);
				lblId.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblId);
			}
			{
				lblNaziv = new JLabel("Naziv");
				sl_panel.putConstraint(SpringLayout.WEST, lblNaziv, 20,
						SpringLayout.WEST, panel);
				sl_panel.putConstraint(SpringLayout.WEST, lblId, 0,
						SpringLayout.WEST, lblNaziv);
				sl_panel.putConstraint(SpringLayout.NORTH, lblNaziv, 1,
						SpringLayout.NORTH, txtNaziv);
				lblNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblNaziv);
			}
			{
				lblKontinent = new JLabel("\u017Dupanija");
				sl_panel.putConstraint(SpringLayout.NORTH, lblKontinent, 31,
						SpringLayout.SOUTH, lblNaziv);
				sl_panel.putConstraint(SpringLayout.WEST, lblKontinent, 20,
						SpringLayout.WEST, panel);
				lblKontinent.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblKontinent);
			}
			{
				txtZupanijaId = new JTextField();
				txtZupanijaId.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						txtZupanijaId.selectAll();
					}
				});
				txtZupanijaId.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						if (e.getKeyCode() == KeyEvent.VK_ENTER) {
							pozoviZupaniju();
						}
					}

					@Override
					public void keyTyped(KeyEvent e) {
						e.consume();
					}
				});
				sl_panel.putConstraint(SpringLayout.NORTH, txtZupanijaId, 25,
						SpringLayout.SOUTH, txtNaziv);
				sl_panel.putConstraint(SpringLayout.WEST, txtZupanijaId, 0,
						SpringLayout.WEST, txtNaziv);
				txtZupanijaId.setText((String) null);
				txtZupanijaId.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtZupanijaId.setColumns(10);
				panel.add(txtZupanijaId);
			}
			{
				txtZupanijaNaziv = new JTextField();
				sl_panel.putConstraint(SpringLayout.WEST, txtZupanijaNaziv, 40,
						SpringLayout.WEST, txtNaziv);
				sl_panel.putConstraint(SpringLayout.EAST, txtZupanijaNaziv,
						-29, SpringLayout.EAST, panel);
				sl_panel.putConstraint(SpringLayout.EAST, txtZupanijaId, 0,
						SpringLayout.WEST, txtZupanijaNaziv);
				sl_panel.putConstraint(SpringLayout.NORTH, txtZupanijaNaziv,
						25, SpringLayout.SOUTH, txtNaziv);
				txtZupanijaNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
				txtZupanijaNaziv.setEditable(false);
				txtZupanijaNaziv.setColumns(10);
				panel.add(txtZupanijaNaziv);
			}
			{
				JButton button = new JButton("");
				sl_panel.putConstraint(SpringLayout.WEST, button, 311,
						SpringLayout.WEST, panel);
				button.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						pozoviZupaniju();
					}
				});
				sl_panel.putConstraint(SpringLayout.NORTH, button, 25,
						SpringLayout.SOUTH, txtNaziv);
				sl_panel.putConstraint(SpringLayout.SOUTH, button, 0,
						SpringLayout.SOUTH, txtZupanijaNaziv);
				sl_panel.putConstraint(SpringLayout.EAST, button, 15,
						SpringLayout.EAST, txtZupanijaNaziv);
				panel.add(button);
			}
		}
		setTitle(grad.getNaziv());
		txtId.setText(grad.getId());
		txtNaziv.setText(grad.getNaziv());
		txtZupanijaId.setText(grad.getZupanija());

		if (grad.getZupanija() != null && grad.getZupanija() != "") {
			SQLManager baza = new SQLManager();
			Vector<String> param = new Vector<String>();
			param.add("id");
			param.add("naziv");
			Vector<Vector<String>> gradV = baza.read("zupanije", param,
					"WHERE id = " + grad.getZupanija());

			txtZupanijaNaziv.setText(gradV.get(0).elementAt(1));
		}
	}

	private void pozoviZupaniju() {
		ZupanijeForma dijalog = new ZupanijeForma();
		dijalog.setVisible(true);
		Zupanija zup = dijalog.getZupanija();
		if (zup != null) {
			txtZupanijaId.setText(zup.getId());
			txtZupanijaNaziv.setText(zup.getNaziv());
		}
	}

}
