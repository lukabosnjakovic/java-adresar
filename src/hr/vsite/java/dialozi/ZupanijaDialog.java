package hr.vsite.java.dialozi;

import hr.vsite.java.forme.DrzaveForma;
import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Drzava;
import hr.vsite.java.tipovi.Zupanija;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Font;
import java.util.TreeMap;
import java.util.Vector;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ZupanijaDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtId;
	private JLabel lblId;
	private JTextField txtNaziv;
	private JTextField txtDrzavaNaziv;
	private JTextField txtDrzavaId;
	private JTextField txtPozivniBroj;
	private JLabel lblPozIzDrz;
	private JButton okButton;
	private JButton cancelButton;
	private JPanel buttonPane;

	/**
	 * Create the dialog.
	 */
	public ZupanijaDialog(Zupanija zupanija, boolean update) {
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 334, 392);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout springLayout = new SpringLayout();
		contentPanel.setLayout(springLayout);
		{
			lblId = new JLabel("ID");
			springLayout.putConstraint(SpringLayout.WEST, lblId, 32,
					SpringLayout.WEST, contentPanel);
			lblId.setFont(new Font("Tahoma", Font.PLAIN, 13));
			contentPanel.add(lblId);
		}

		txtId = new JTextField();
		txtId.setEnabled(false);
		txtId.setEditable(false);
		springLayout.putConstraint(SpringLayout.WEST, txtId, -200,
				SpringLayout.EAST, contentPanel);
		springLayout.putConstraint(SpringLayout.EAST, txtId, -32,
				SpringLayout.EAST, contentPanel);
		springLayout.putConstraint(SpringLayout.NORTH, lblId, 3,
				SpringLayout.NORTH, txtId);
		txtId.setFont(new Font("Tahoma", Font.PLAIN, 13));
		springLayout.putConstraint(SpringLayout.NORTH, txtId, 40,
				SpringLayout.NORTH, contentPanel);
		contentPanel.add(txtId);
		txtId.setColumns(10);

		JLabel lblNaziv = new JLabel("Naziv");
		springLayout.putConstraint(SpringLayout.WEST, lblNaziv, 0,
				SpringLayout.WEST, lblId);
		lblNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblNaziv);

		JLabel lblDrava = new JLabel("Dr\u017Eava");
		springLayout.putConstraint(SpringLayout.WEST, lblDrava, 0,
				SpringLayout.WEST, lblId);
		lblDrava.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblDrava);

		JLabel lblPozivniBroj = new JLabel("Pozivni Broj");
		springLayout.putConstraint(SpringLayout.WEST, lblPozivniBroj, 0,
				SpringLayout.WEST, lblId);
		lblPozivniBroj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblPozivniBroj);

		txtNaziv = new JTextField();
		txtNaziv.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				txtNaziv.selectAll();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, lblNaziv, 3,
				SpringLayout.NORTH, txtNaziv);
		springLayout.putConstraint(SpringLayout.NORTH, txtNaziv, 25,
				SpringLayout.SOUTH, txtId);
		springLayout.putConstraint(SpringLayout.WEST, txtNaziv, 0,
				SpringLayout.WEST, txtId);
		springLayout.putConstraint(SpringLayout.EAST, txtNaziv, 0,
				SpringLayout.EAST, txtId);
		txtNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtNaziv.setColumns(10);
		contentPanel.add(txtNaziv);

		txtDrzavaNaziv = new JTextField();
		txtDrzavaNaziv.setEditable(false);
		springLayout.putConstraint(SpringLayout.NORTH, lblDrava, 3,
				SpringLayout.NORTH, txtDrzavaNaziv);
		springLayout.putConstraint(SpringLayout.NORTH, txtDrzavaNaziv, 25,
				SpringLayout.SOUTH, txtNaziv);
		springLayout.putConstraint(SpringLayout.EAST, txtDrzavaNaziv, 0,
				SpringLayout.EAST, txtId);
		txtDrzavaNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtDrzavaNaziv.setColumns(10);
		contentPanel.add(txtDrzavaNaziv);

		txtDrzavaId = new JTextField();
		txtDrzavaId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					pozoviDrzavu();
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				e.consume();
			}
		});
		txtDrzavaId.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtDrzavaId.selectAll();
			}
		});
		springLayout.putConstraint(SpringLayout.WEST, txtDrzavaNaziv, 0,
				SpringLayout.EAST, txtDrzavaId);
		springLayout.putConstraint(SpringLayout.NORTH, txtDrzavaId, 25,
				SpringLayout.SOUTH, txtNaziv);
		springLayout.putConstraint(SpringLayout.WEST, txtDrzavaId, 0,
				SpringLayout.WEST, txtNaziv);
		springLayout.putConstraint(SpringLayout.EAST, txtDrzavaId, -125,
				SpringLayout.EAST, txtNaziv);
		txtDrzavaId.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtDrzavaId.setColumns(10);
		contentPanel.add(txtDrzavaId);

		txtPozivniBroj = new JTextField();
		txtPozivniBroj.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtPozivniBroj.selectAll();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, lblPozivniBroj, 3,
				SpringLayout.NORTH, txtPozivniBroj);
		springLayout.putConstraint(SpringLayout.NORTH, txtPozivniBroj, 25,
				SpringLayout.SOUTH, txtDrzavaNaziv);
		springLayout.putConstraint(SpringLayout.WEST, txtPozivniBroj, 0,
				SpringLayout.WEST, txtNaziv);
		springLayout.putConstraint(SpringLayout.EAST, txtPozivniBroj, 0,
				SpringLayout.EAST, txtId);
		txtPozivniBroj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtPozivniBroj.setColumns(10);
		contentPanel.add(txtPozivniBroj);

		JLabel lblPozivniBrojIzvan = new JLabel(
				"Pozivni Broj Izvan Dr\u017Eave:");
		springLayout.putConstraint(SpringLayout.NORTH, lblPozivniBrojIzvan, 39,
				SpringLayout.SOUTH, lblPozivniBroj);
		springLayout.putConstraint(SpringLayout.WEST, lblPozivniBrojIzvan, 0,
				SpringLayout.WEST, lblId);
		lblPozivniBrojIzvan.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblPozivniBrojIzvan);

		lblPozIzDrz = new JLabel("");
		springLayout.putConstraint(SpringLayout.NORTH, lblPozIzDrz, 0,
				SpringLayout.NORTH, lblPozivniBrojIzvan);
		springLayout.putConstraint(SpringLayout.WEST, lblPozIzDrz, 6,
				SpringLayout.EAST, lblPozivniBrojIzvan);
		lblPozIzDrz.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.add(lblPozIzDrz);
		{
			buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if(txtNaziv.getText() != ""){
							String naredba = "";
							if (update)
								naredba = "UPDATE zupanije SET naziv = ?, drzavaID = ?, pozivniBroj = ? WHERE id = "
										+ txtId.getText() + ";";
							else
								naredba = "INSERT INTO	zupanije (naziv, drzavaID, pozivniBroj) VALUES ( ? , ? , ? );";

							TreeMap<Integer, String> param = new TreeMap<Integer, String>();
							param.put(1, txtNaziv.getText());
							param.put(2, txtDrzavaId.getText());
							param.put(3, txtPozivniBroj.getText());
							SQLManager baza = new SQLManager();
							baza.send(naredba, param);
							dispose();
						}						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setTitle(zupanija.getNaziv());
		txtId.setText(zupanija.getId());
		txtNaziv.setText(zupanija.getNaziv());
		txtDrzavaId.setText(zupanija.getDrzava());
		txtPozivniBroj.setText(zupanija.getPozivniBroj());

		if (zupanija.getDrzava() != null && zupanija.getDrzava() != "") {
			SQLManager baza = new SQLManager();
			Vector<String> param = new Vector<String>();
			param.add("id");
			param.add("naziv");
			param.add("pozivniBroj");
			Vector<Vector<String>> drzava = baza.read("drzave", param,
					"WHERE id = " + zupanija.getDrzava());

			txtDrzavaNaziv.setText(drzava.get(0).elementAt(1));
			lblPozIzDrz.setText(drzava.get(0).elementAt(2) + " "
					+ zupanija.getPozivniBroj().substring(1));
		}

		JButton btnDrzava = new JButton("");
		btnDrzava.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				pozoviDrzavu();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnDrzava, 0,
				SpringLayout.NORTH, txtDrzavaNaziv);
		springLayout.putConstraint(SpringLayout.WEST, btnDrzava, 0,
				SpringLayout.EAST, txtDrzavaNaziv);
		springLayout.putConstraint(SpringLayout.SOUTH, btnDrzava, 0,
				SpringLayout.SOUTH, txtDrzavaNaziv);
		springLayout.putConstraint(SpringLayout.EAST, btnDrzava, 15,
				SpringLayout.EAST, txtDrzavaNaziv);
		contentPanel.add(btnDrzava);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] {
				txtNaziv, txtDrzavaId, btnDrzava, txtPozivniBroj, okButton,
				cancelButton, contentPanel, getContentPane(), lblId, txtId,
				lblNaziv, lblDrava, lblPozivniBroj, txtDrzavaNaziv,
				lblPozivniBrojIzvan, lblPozIzDrz, buttonPane }));
	}

	private void pozoviDrzavu() {
		DrzaveForma dijalog = new DrzaveForma();
		dijalog.setVisible(true);
		Drzava drv = dijalog.getDrzava();
		if (drv != null) {
			txtDrzavaId.setText(drv.getId());
			txtDrzavaNaziv.setText(drv.getNaziv());
			if (txtPozivniBroj.getText().length() > 0)
				lblPozIzDrz.setText(drv.getPozivniBroj() + " "
						+ txtPozivniBroj.getText().substring(1));
		}
	}
}
