package hr.vsite.java.dialozi;

import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Drzava;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.TreeMap;

public class DrzavaDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtId;
	private JTextField txtNaziv;
	private JTextField txtKontinent;
	private JTextField txtPozivniBr;
	private JLabel lblId;

	/**
	 * Create the dialog.
	 */
	public DrzavaDialog(Drzava drzava, boolean update) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setBounds(100, 100, 318, 354);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if(txtNaziv.getText() != ""){
							String naredba = "";
							if(update)
								naredba = "UPDATE drzave SET naziv = ?, kontinent = ?, pozivniBroj = ? WHERE id = "+ txtId.getText() + ";";
							else
								naredba = "INSERT INTO drzave(naziv, kontinent, pozivniBroj) VALUES ( ? , ? , ? );";
							
							TreeMap<Integer,String> param = new TreeMap<Integer, String>();
							param.put(1, txtNaziv.getText());
							param.put(2, txtKontinent.getText());
							param.put(3, txtPozivniBr.getText());
							SQLManager baza = new SQLManager();
							baza.send(naredba, param);
							dispose();
						}						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.CENTER);
			SpringLayout sl_panel = new SpringLayout();
			panel.setLayout(sl_panel);
			
			txtId = new JTextField();
			txtId.setEditable(false);
			txtId.setEnabled(false);
			txtId.setFont(new Font("Tahoma", Font.PLAIN, 13));
			sl_panel.putConstraint(SpringLayout.NORTH, txtId, 59, SpringLayout.NORTH, panel);
			sl_panel.putConstraint(SpringLayout.WEST, txtId, -167, SpringLayout.EAST, panel);
			sl_panel.putConstraint(SpringLayout.EAST, txtId, -29, SpringLayout.EAST, panel);
			panel.add(txtId);
			txtId.setColumns(10);
			
			txtNaziv = new JTextField();
			txtNaziv.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					txtNaziv.selectAll();
				}
			});
			txtNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
			sl_panel.putConstraint(SpringLayout.NORTH, txtNaziv, 25, SpringLayout.SOUTH, txtId);
			sl_panel.putConstraint(SpringLayout.WEST, txtNaziv, 0, SpringLayout.WEST, txtId);
			sl_panel.putConstraint(SpringLayout.EAST, txtNaziv, 0, SpringLayout.EAST, txtId);
			panel.add(txtNaziv);
			txtNaziv.setColumns(10);
			{
				txtKontinent = new JTextField();
				txtKontinent.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent e) {
						txtKontinent.selectAll();
					}
				});
				txtKontinent.setFont(new Font("Tahoma", Font.PLAIN, 13));
				sl_panel.putConstraint(SpringLayout.NORTH, txtKontinent, 25, SpringLayout.SOUTH, txtNaziv);
				sl_panel.putConstraint(SpringLayout.WEST, txtKontinent, 0, SpringLayout.WEST, txtNaziv);
				sl_panel.putConstraint(SpringLayout.EAST, txtKontinent, 0, SpringLayout.EAST, txtId);
				panel.add(txtKontinent);
				txtKontinent.setColumns(10);
			}
			{
				txtPozivniBr = new JTextField();
				txtPozivniBr.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent e) {
						txtPozivniBr.selectAll();
					}
				});
				txtPozivniBr.setFont(new Font("Tahoma", Font.PLAIN, 13));
				sl_panel.putConstraint(SpringLayout.NORTH, txtPozivniBr, 25, SpringLayout.SOUTH, txtKontinent);
				sl_panel.putConstraint(SpringLayout.WEST, txtPozivniBr, 0, SpringLayout.WEST, txtKontinent);
				sl_panel.putConstraint(SpringLayout.EAST, txtPozivniBr, 0, SpringLayout.EAST, txtId);
				panel.add(txtPozivniBr);
				txtPozivniBr.setColumns(10);
			}
			{
				lblId = new JLabel("ID");
				sl_panel.putConstraint(SpringLayout.NORTH, lblId, 1, SpringLayout.NORTH, txtId);
				lblId.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblId);
			}
			{
				JLabel lblNaziv = new JLabel("Naziv");
				sl_panel.putConstraint(SpringLayout.WEST, lblNaziv, 20, SpringLayout.WEST, panel);
				sl_panel.putConstraint(SpringLayout.WEST, lblId, 0, SpringLayout.WEST, lblNaziv);
				sl_panel.putConstraint(SpringLayout.NORTH, lblNaziv, 1, SpringLayout.NORTH, txtNaziv);
				lblNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblNaziv);
			}
			{
				JLabel lblKontinent = new JLabel("Kontinent");
				sl_panel.putConstraint(SpringLayout.NORTH, lblKontinent, 1, SpringLayout.NORTH, txtKontinent);
				sl_panel.putConstraint(SpringLayout.WEST, lblKontinent, 20, SpringLayout.WEST, panel);
				lblKontinent.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblKontinent);
			}
			{
				JLabel lblPozivniBroj = new JLabel("Pozivni broj");
				sl_panel.putConstraint(SpringLayout.NORTH, lblPozivniBroj, 1, SpringLayout.NORTH, txtPozivniBr);
				sl_panel.putConstraint(SpringLayout.WEST, lblPozivniBroj, 20, SpringLayout.WEST, panel);
				lblPozivniBroj.setFont(new Font("Tahoma", Font.PLAIN, 13));
				panel.add(lblPozivniBroj);
			}			
		}
		setTitle(drzava.getNaziv());
		txtId.setText(drzava.getId());
		txtNaziv.setText(drzava.getNaziv());
		txtKontinent.setText(drzava.getKontinent());
		txtPozivniBr.setText(drzava.getPozivniBroj());
	}
	
}
