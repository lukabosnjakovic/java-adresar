package hr.vsite.java.dialozi;

import hr.vsite.java.pomagala.MyLogger;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Vector;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class PostavkeDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JTextField txtHost;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PostavkeDialog dialog = new PostavkeDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PostavkeDialog() {
		setTitle("Postavke");
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 406, 334);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txtUsername = new JTextField();
			txtUsername.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					txtUsername.selectAll();
				}
			});
			txtUsername.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtUsername.setBounds(188, 62, 142, 20);
			contentPanel.add(txtUsername);
			txtUsername.setColumns(10);
		}
		{
			JLabel lblUsername = new JLabel("Username:");
			lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lblUsername.setBounds(46, 64, 72, 17);
			contentPanel.add(lblUsername);
		}
		{
			JLabel lblPassword = new JLabel("Password:");
			lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lblPassword.setBounds(46, 109, 72, 17);
			contentPanel.add(lblPassword);
		}
		{
			JLabel lblHost = new JLabel("Host:");
			lblHost.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lblHost.setBounds(46, 159, 72, 17);
			contentPanel.add(lblHost);
		}
		{
			txtPassword = new JTextField();
			txtPassword.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					txtPassword.selectAll();
				}
			});
			txtPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtPassword.setColumns(10);
			txtPassword.setBounds(188, 108, 142, 20);
			contentPanel.add(txtPassword);
		}
		{
			txtHost = new JTextField();
			txtHost.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					txtHost.selectAll();
				}
			});
			txtHost.setFont(new Font("Tahoma", Font.PLAIN, 13));
			txtHost.setColumns(10);
			txtHost.setBounds(188, 158, 142, 20);
			contentPanel.add(txtHost);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						saveParamChanges();
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		Vector<String> param = loadParams();
		if(param != null){
			txtHost.setText(param.elementAt(0));
			txtUsername.setText(param.elementAt(1));
			txtPassword.setText(param.elementAt(2));
		}
	}

	public Vector<String> loadParams() {
		Properties props = new Properties();
		InputStream is = null;
		Vector<String> pro = null;
		try {
			File f = new File("Adresar.properties");
			is = new FileInputStream(f);
		} 
		catch (Exception e) {
			MyLogger.log.error("Gre�ka kod otvarnja datoteke", e);
			is = null;
		}
		try {

			props.load(is);
			pro = new Vector<String>();
			pro.add(props.getProperty("host"));
			pro.add(props.getProperty("username"));
			pro.add(props.getProperty("password"));
		} 
		catch (Exception e) {
		}		
		return pro;
	}

	public void saveParamChanges() {
		try {
			Properties props = new Properties();
			props.setProperty("host", txtHost.getText());
			props.setProperty("username", txtUsername.getText());
			props.setProperty("password", txtPassword.getText());
			File f = new File("Adresar.properties");
			OutputStream out = new FileOutputStream(f);
			props.store(out, "Adresar postavke:");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
