package hr.vsite.java.dialozi;

import hr.vsite.java.forme.GradoviForma;
import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Grad;
import hr.vsite.java.tipovi.Kontakt;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.TreeMap;
import java.util.Vector;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class KontaktDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtIme;
	private JTextField txtPrezime;
	private JTextField txtID;
	private JTextField txtUlica;
	private JTextField txtBroj;
	private JTextField txtDodatak;
	private JTextField txtGradId;
	private JTextField txtGradNaziv;
	private JTextField txtKucni;
	private JTextField txtMobitel;
	private JTextField txtPosao;
	private JLabel lblPozivniBroj;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Kontakt kon = new Kontakt();
			KontaktDialog dialog = new KontaktDialog(kon, false);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public KontaktDialog(Kontakt kontakt, boolean update) {
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 534, 468);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		txtIme = new JTextField();
		txtIme.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				txtIme.selectAll();
			}
		});
		txtIme.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtIme.setBounds(185, 67, 102, 20);
		contentPanel.add(txtIme);
		txtIme.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblId.setBounds(37, 29, 46, 14);
		contentPanel.add(lblId);

		JLabel lblIme = new JLabel("Ime");
		lblIme.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblIme.setBounds(37, 70, 46, 14);
		contentPanel.add(lblIme);

		JLabel lblPrezime = new JLabel("Prezime");
		lblPrezime.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPrezime.setBounds(67, 70, 74, 14);
		contentPanel.add(lblPrezime);

		txtPrezime = new JTextField();
		txtPrezime.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtPrezime.selectAll();
			}
		});
		txtPrezime.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtPrezime.setColumns(10);
		txtPrezime.setBounds(297, 67, 146, 20);
		contentPanel.add(txtPrezime);

		txtID = new JTextField();
		txtID.setEditable(false);
		txtID.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtID.setColumns(10);
		txtID.setBounds(185, 26, 102, 20);
		contentPanel.add(txtID);

		txtUlica = new JTextField();
		txtUlica.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtUlica.selectAll();
			}
		});
		txtUlica.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtUlica.setColumns(10);
		txtUlica.setBounds(185, 114, 190, 20);
		contentPanel.add(txtUlica);

		txtBroj = new JTextField();
		txtBroj.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtBroj.selectAll();
			}
		});
		txtBroj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtBroj.setColumns(10);
		txtBroj.setBounds(385, 114, 58, 20);
		contentPanel.add(txtBroj);

		JLabel lblUlica = new JLabel("Ulica");
		lblUlica.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblUlica.setBounds(37, 117, 46, 14);
		contentPanel.add(lblUlica);

		JLabel lblBroj = new JLabel("Broj");
		lblBroj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblBroj.setBounds(70, 117, 46, 14);
		contentPanel.add(lblBroj);

		txtDodatak = new JTextField();
		txtDodatak.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtDodatak.selectAll();
			}
		});
		txtDodatak.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtDodatak.setColumns(10);
		txtDodatak.setBounds(185, 164, 258, 20);
		contentPanel.add(txtDodatak);

		JLabel lblDodatakAdresi = new JLabel("Dodatak adresi");
		lblDodatakAdresi.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblDodatakAdresi.setBounds(37, 169, 120, 14);
		contentPanel.add(lblDodatakAdresi);

		JLabel lblRa = new JLabel("Grad");
		lblRa.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRa.setBounds(37, 212, 120, 14);
		contentPanel.add(lblRa);

		txtGradId = new JTextField();
		txtGradId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				e.consume();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					pozoviGrad();
				}
			}
		});
		txtGradId.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtGradId.selectAll();
			}
		});
		txtGradId.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtGradId.setColumns(10);
		txtGradId.setBounds(185, 206, 36, 20);
		contentPanel.add(txtGradId);

		txtGradNaziv = new JTextField();
		txtGradNaziv.setEditable(false);
		txtGradNaziv.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtGradNaziv.setColumns(10);
		txtGradNaziv.setBounds(231, 206, 212, 20);
		contentPanel.add(txtGradNaziv);

		JLabel lblKuni = new JLabel("Ku\u0107ni");
		lblKuni.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblKuni.setBounds(37, 256, 120, 14);
		contentPanel.add(lblKuni);

		JLabel lblMobitel = new JLabel("Mobitel");
		lblMobitel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblMobitel.setBounds(37, 298, 120, 14);
		contentPanel.add(lblMobitel);

		JLabel lblPosao = new JLabel("Posao");
		lblPosao.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPosao.setBounds(39, 340, 120, 14);
		contentPanel.add(lblPosao);

		txtKucni = new JTextField();
		txtKucni.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtKucni.selectAll();
			}
		});
		txtKucni.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtKucni.setColumns(10);
		txtKucni.setBounds(185, 253, 156, 20);
		contentPanel.add(txtKucni);

		txtMobitel = new JTextField();
		txtMobitel.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtMobitel.selectAll();
			}
		});
		txtMobitel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtMobitel.setColumns(10);
		txtMobitel.setBounds(185, 295, 156, 20);
		contentPanel.add(txtMobitel);

		txtPosao = new JTextField();
		txtPosao.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtPosao.selectAll();
			}
		});
		txtPosao.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtPosao.setColumns(10);
		txtPosao.setBounds(185, 337, 156, 20);
		contentPanel.add(txtPosao);

		JButton button = new JButton("");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				pozoviGrad();
			}
		});
		button.setBounds(449, 205, 15, 23);
		contentPanel.add(button);

		lblPozivniBroj = new JLabel((String) null);
		lblPozivniBroj.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPozivniBroj.setBounds(385, 256, 110, 14);
		contentPanel.add(lblPozivniBroj);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if(txtIme.getText() != ""|| txtPrezime.getText() != ""){
							String naredba = "";
							if (update)
								naredba = "UPDATE kontakti SET ime = ?, prezime = ?, ulica = ?, broj = ?, dodatak = ?, gradID = ?, kucni = ?, mobitel = ?, posao = ? WHERE id = "
										+ txtID.getText() + ";";
							else
								naredba = "INSERT INTO kontakti (ime, prezime, ulica, broj, dodatak, gradID, kucni, mobitel, posao) "
										+ "VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ?);";

							TreeMap<Integer, String> param = new TreeMap<Integer, String>();
							param.put(1, txtIme.getText());
							param.put(2, txtPrezime.getText());
							param.put(3, txtUlica.getText());
							param.put(4, txtBroj.getText());
							param.put(5, txtDodatak.getText());
							param.put(6, txtGradId.getText());
							param.put(7, txtKucni.getText());
							param.put(8, txtMobitel.getText());
							param.put(9, txtPosao.getText());
							SQLManager baza = new SQLManager();
							baza.send(naredba, param);
							dispose();
						}						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		txtID.setText(kontakt.getId());
		txtIme.setText(kontakt.getIme());
		txtPrezime.setText(kontakt.getPrezime());
		txtUlica.setText(kontakt.getUlica());
		txtBroj.setText(kontakt.getBroj());
		txtDodatak.setText(kontakt.getdodatak());
		txtGradId.setText(kontakt.getGrad());
		
		txtKucni.setText(kontakt.getKucni());
		txtMobitel.setText(kontakt.getMobitel());
		txtPosao.setText(kontakt.getPosao());
		if(kontakt.getGrad() != null && kontakt.getGrad()  != ""){
			SQLManager baza = new SQLManager();
			Vector<String> param = new Vector<String>();
			param.add("naziv");
			Vector<Vector<String>> table0 = baza
					.read("SELECT naziv FROM gradovi WHERE id = " + kontakt.getGrad(), param);
			txtGradNaziv.setText(table0.elementAt(0).elementAt(0));
		}	
	}

	private void pozoviGrad() {
		GradoviForma dijalog = new GradoviForma();
		dijalog.setVisible(true);
		Grad gr = dijalog.getGrad();
		if (gr != null) {
			txtGradId.setText(gr.getId());
			txtGradNaziv.setText(gr.getNaziv());
			SQLManager baza = new SQLManager();
			Vector<String> param = new Vector<String>();
			param.add("z.pozivniBroj");
			param.add("d.pozivniBroj");
			Vector<Vector<String>> table = baza
					.read("SELECT z.pozivniBroj, d.pozivniBroj FROM gradovi g, zupanije z, drzave d WHERE g.zupanijaID = z.id AND z.drzavaID = d.id AND g.id = "
							+ gr.getId() + " LIMIT 100", param);
			lblPozivniBroj.setText(table.elementAt(0).elementAt(1) + " "
					+ table.elementAt(0).elementAt(0).substring(1));
		}
	}
}
