package hr.vsite.java.forme;

import hr.vsite.java.dialozi.DrzavaDialog;
import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Drzava;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.SpringLayout;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.util.Vector;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class DrzaveForma extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private JButton btnUredi;
	private JButton btnIzbrii;
	private Drzava odabranaDrzava;
	

	/**
	 * Create the dialog.
	 */
	public DrzaveForma() {
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Dr\u017Eave");
		setResizable(false);
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 569, 418);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);
		
		JPanel panel = new JPanel();
		sl_contentPanel.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, panel, 305, SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, contentPanel);
		contentPanel.add(panel);
		
		
		Vector<String> param = new Vector<String>();
		param.add("id");
		param.add("naziv");
		param.add("kontinent");
		param.add("pozivniBroj");
		Vector<Vector<String>> table0 = puniDrzave(param);
		param.clear();
		param.add("ID");
		param.add("Naziv");
		param.add("Kontinent");
		param.add("Pozivni Broj");
		DefaultTableModel model = new DefaultTableModel(table0, param){

		    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public boolean isCellEditable(int i, int i1) {
		        return false; //To change body of generated methods, choose Tools | Templates.
		    }

		   };
		table = new JTable(model);
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){					
					odaberiDrzavu();
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(table.getSelectedRow()> -1){
					btnUredi.setEnabled(true);
					btnIzbrii.setEnabled(true);
					if(e.getClickCount() == 2)
						uredi();
				}
				
			}
		});
		table.setFont(new Font("Tahoma", Font.PLAIN, 13));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setPreferredScrollableViewportSize(new Dimension(530, 288));
		table.setFillsViewportHeight(true);

		JScrollPane scrollPane = new JScrollPane(table);
		
		panel.add(scrollPane);
		
		JButton btnNovi = new JButton("Novi");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, btnNovi, 8, SpringLayout.SOUTH, panel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, btnNovi, -83, SpringLayout.EAST, contentPanel);
		btnNovi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Drzava drv = new Drzava();
				DrzavaDialog d = new DrzavaDialog(drv, false);
				d.setVisible(true);
				restartTable();
			}
		});
		contentPanel.add(btnNovi);
		
		btnUredi = new JButton("Uredi");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, btnUredi, 0, SpringLayout.NORTH, btnNovi);
		sl_contentPanel.putConstraint(SpringLayout.WEST, btnUredi, 6, SpringLayout.EAST, btnNovi);
		sl_contentPanel.putConstraint(SpringLayout.EAST, btnUredi, -11, SpringLayout.EAST, contentPanel);
		btnUredi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(btnUredi.isEnabled()){
					uredi();
				}
			}
		});
		contentPanel.add(btnUredi);
		
		btnIzbrii = new JButton("Izbri\u0161i");
		sl_contentPanel.putConstraint(SpringLayout.EAST, btnIzbrii, 90, SpringLayout.WEST, panel);
		btnIzbrii.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(btnIzbrii.isEnabled()){
					SQLManager baza = new SQLManager();
					int row = table.getSelectedRow();
					baza.delete("drzave", table.getValueAt(row, 0).toString());
					restartTable();
				}				
			}
		});
		sl_contentPanel.putConstraint(SpringLayout.WEST, btnIzbrii, 10, SpringLayout.WEST, panel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, btnIzbrii, 0, SpringLayout.SOUTH, btnNovi);
		contentPanel.add(btnIzbrii);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnOdabir = new JButton("Odabir");
				btnOdabir.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						odaberiDrzavu();
					}
				});
				btnOdabir.setActionCommand("Cancel");
				buttonPane.add(btnOdabir);
			}
		}
		btnUredi.setEnabled(false);
		btnIzbrii.setEnabled(false);
		
	}
	
	private void odaberiDrzavu(){
		if (table.getSelectedRow() != -1) {
			odabranaDrzava = new Drzava();
			int row = table.getSelectedRow();
			odabranaDrzava.setId(table.getValueAt(row, 0).toString());
			odabranaDrzava.setNaziv(table.getValueAt(row, 1).toString());
			odabranaDrzava.setKontinent(table.getValueAt(row, 2).toString());
			odabranaDrzava.setPozivniBroj(table.getValueAt(row, 3).toString());
			setVisible(false);
		}		
	}
	
	private Vector<Vector<String>> puniDrzave(Vector<String> param){
		SQLManager baza = new SQLManager();		
		Vector<Vector<String>> table = baza.read("drzave", param, "");
		return table;
	}
	
	private void restartTable(){
		dispose();
		DrzaveForma dialog = new DrzaveForma();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}
	
	public Drzava getDrzava(){
		if(odabranaDrzava != null){
			dispose();
			return odabranaDrzava;
		}
		dispose();
		return null;
	}
	
	private void uredi(){
		Drzava drzava = new Drzava();
		int row = table.getSelectedRow();
		drzava.setId(table.getValueAt(row, 0).toString());
		drzava.setNaziv(table.getValueAt(row, 1).toString());
		drzava.setKontinent(table.getValueAt(row, 2).toString());
		drzava.setPozivniBroj(table.getValueAt(row, 3).toString());
		DrzavaDialog dDialog = new DrzavaDialog(drzava, true);
		dDialog.setVisible(true);
		restartTable();
	}
}
