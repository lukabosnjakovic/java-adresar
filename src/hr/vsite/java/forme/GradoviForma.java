package hr.vsite.java.forme;

import hr.vsite.java.dialozi.GradDialog;
import hr.vsite.java.pomagala.SQLManager;
import hr.vsite.java.tipovi.Grad;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class GradoviForma extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private JButton btnUredi;
	private JButton btnIzbrii;
	private Grad odabraniGrad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			GradoviForma dialog = new GradoviForma();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public GradoviForma() {
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Gradovi");
		setResizable(false);
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 569, 418);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);

		JPanel panel = new JPanel();
		sl_contentPanel.putConstraint(SpringLayout.NORTH, panel, 0,
				SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, panel, 0,
				SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, panel, 305,
				SpringLayout.NORTH, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, panel, 0,
				SpringLayout.EAST, contentPanel);
		contentPanel.add(panel);

		Vector<String> param = new Vector<String>();
		param.add("g.id");
		param.add("g.naziv");
		param.add("z.naziv");
		param.add("d.naziv");
		Vector<Vector<String>> table0 = puniGradove(param);
		param.clear();
		param.add("ID");
		param.add("Naziv");
		param.add("�upanija");
		param.add("Dr�ava");
		DefaultTableModel model = new DefaultTableModel(table0, param) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int i, int i1) {
				return false; // To change body of generated methods, choose
								// Tools | Templates.
			}

		};
		table = new JTable(model);
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (table.getSelectedRow() != -1)
						odaberiGrad();
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRow() > -1) {
					btnUredi.setEnabled(true);
					btnIzbrii.setEnabled(true);
					if(e.getClickCount() == 2)
						uredi();
				}

			}
		});
		table.setFont(new Font("Tahoma", Font.PLAIN, 13));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setPreferredScrollableViewportSize(new Dimension(530, 288));
		table.setFillsViewportHeight(true);

		JScrollPane scrollPane = new JScrollPane(table);

		panel.add(scrollPane);

		JButton btnNovi = new JButton("Novi");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, btnNovi, 8,
				SpringLayout.SOUTH, panel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, btnNovi, -83,
				SpringLayout.EAST, contentPanel);
		btnNovi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Grad gr = new Grad();
				GradDialog d = new GradDialog(gr, false);
				d.setVisible(true);
				restartTable();
			}
		});
		contentPanel.add(btnNovi);

		btnUredi = new JButton("Uredi");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, btnUredi, 0,
				SpringLayout.NORTH, btnNovi);
		sl_contentPanel.putConstraint(SpringLayout.WEST, btnUredi, 6,
				SpringLayout.EAST, btnNovi);
		sl_contentPanel.putConstraint(SpringLayout.EAST, btnUredi, -11,
				SpringLayout.EAST, contentPanel);
		btnUredi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (btnUredi.isEnabled()) {
					uredi();
				}
			}
		});
		contentPanel.add(btnUredi);

		btnIzbrii = new JButton("Izbri\u0161i");
		sl_contentPanel.putConstraint(SpringLayout.EAST, btnIzbrii, 90,
				SpringLayout.WEST, panel);
		btnIzbrii.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (btnIzbrii.isEnabled()) {
					SQLManager baza = new SQLManager();
					int row = table.getSelectedRow();
					baza.delete("gradovi", table.getValueAt(row, 0).toString());
					restartTable();
				}
			}
		});
		sl_contentPanel.putConstraint(SpringLayout.WEST, btnIzbrii, 10,
				SpringLayout.WEST, panel);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, btnIzbrii, 0,
				SpringLayout.SOUTH, btnNovi);
		contentPanel.add(btnIzbrii);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnOdabir = new JButton("Odabir");
				btnOdabir.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						odaberiGrad();
					}
				});
				btnOdabir.setActionCommand("Cancel");
				buttonPane.add(btnOdabir);
			}
		}
		btnUredi.setEnabled(false);
		btnIzbrii.setEnabled(false);

	}

	private void odaberiGrad() {
		if (table.getSelectedRow() != -1) {
			odabraniGrad = new Grad();
			int row = table.getSelectedRow();
			odabraniGrad.setId(table.getValueAt(row, 0).toString());
			odabraniGrad.setNaziv(table.getValueAt(row, 1).toString());
			odabraniGrad.setZupanija(table.getValueAt(row, 2).toString());
			setVisible(false);
		}
	}

	private Vector<Vector<String>> puniGradove(Vector<String> param) {
		SQLManager baza = new SQLManager();
		Vector<Vector<String>> table = baza
				.read("SELECT g.id, g.naziv, z.naziv, d.naziv FROM gradovi g, zupanije z, drzave d WHERE g.zupanijaID = z.id AND z.drzavaID = d.id LIMIT 100",
						param);
		return table;
	}

	private void restartTable() {
		dispose();
		GradoviForma dialog = new GradoviForma();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}

	public Grad getGrad() {
		if (odabraniGrad != null) {
			dispose();
			return odabraniGrad;
		}
		dispose();
		return null;
	}
	
	private void uredi(){
		Grad grad = new Grad();
		int row = table.getSelectedRow();
		Vector<String> param = new Vector<String>();
		param.add("id");
		param.add("naziv");
		param.add("zupanijaID");
		SQLManager baza = new SQLManager();
		Vector<Vector<String>> gr = baza
				.read("gradovi", param, "WHERE id = "
						+ table.getValueAt(row, 0).toString());
		grad.setId(gr.elementAt(0).elementAt(0).toString());
		grad.setNaziv(gr.elementAt(0).elementAt(1).toString());
		grad.setZupanija(gr.elementAt(0).elementAt(2).toString());
		GradDialog gDialog = new GradDialog(grad, true);
		gDialog.setVisible(true);
		restartTable();
	}
}
