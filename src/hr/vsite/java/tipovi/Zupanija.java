package hr.vsite.java.tipovi;

/**
 * 
 * @author Luka Bošnjaković
 * Klasa reprezentira jednu županiju
 */
public class Zupanija {
	
	private String id;
	private String naziv;
	private String drzavaID;
	private String pozivniBroj;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getDrzava() {
		return drzavaID;
	}
	public void setDrzava(String drzava) {
		this.drzavaID = drzava;
	}
	public String getPozivniBroj() {
		return pozivniBroj;
	}
	public void setPozivniBroj(String pozivniBroj) {
		this.pozivniBroj = pozivniBroj;
	}
}
