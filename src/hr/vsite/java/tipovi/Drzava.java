package hr.vsite.java.tipovi;

/**
 * 
 * @author Luka Bošnjaković
 * Klasa reprezentira jednu državu 
 */
public class Drzava {
	private String id;
	private String naziv;
	private String kontinent;
	private String pozivniBroj;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getKontinent() {
		return kontinent;
	}
	public void setKontinent(String kontinent) {
		this.kontinent = kontinent;
	}
	public String getPozivniBroj() {
		return pozivniBroj;
	}
	public void setPozivniBroj(String pozivniBroj) {
		this.pozivniBroj = pozivniBroj;
	}	
}
