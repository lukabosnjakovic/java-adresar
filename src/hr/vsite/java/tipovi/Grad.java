package hr.vsite.java.tipovi;

/**
 * 
 * @author Luka Bošnjaković
 * Klasa reprezentira jedan Grad
 */
public class Grad {
	
	private String id;
	private String naziv;
	private String zupanijaID;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getZupanija() {
		return zupanijaID;
	}
	public void setZupanija(String zupanija) {
		this.zupanijaID = zupanija;
	}
}
