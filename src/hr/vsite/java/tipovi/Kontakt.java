package hr.vsite.java.tipovi;

/**
 * 
 * @author Luka Bošnjaković
 * Klasa reprezentira jedan kontakt
 */
public class Kontakt {
	private String id;
	private String ime;
	private String prezime;
	private String ulica;
	private String broj;
	private String dodatak;
	private String gradID;
	private String kucni;
	private String mobitel;
	private String posao;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public String getBroj() {
		return broj;
	}
	public void setBroj(String broj) {
		this.broj = broj;
	}
	public String getdodatak() {
		return dodatak;
	}
	public void setdodatak(String dodatak) {
		this.dodatak = dodatak;
	}
	public String getGrad() {
		return gradID;
	}
	public void setGrad(String gradId) {
		this.gradID = gradId;
	}
	public String getKucni() {
		return kucni;
	}
	public void setKucni(String kucni) {
		this.kucni = kucni;
	}
	public String getMobitel() {
		return mobitel;
	}
	public void setMobitel(String mobitel) {
		this.mobitel = mobitel;
	}
	public String getPosao() {
		return posao;
	}
	public void setPosao(String posao) {
		this.posao = posao;
	}
}
